## Laravel

### Installation

`1. composer i` -> is used to install packages that are located in `composer.json`

`2. cp .env.example .env` -> create a new file named `.env` that has content of `.env.example`

`3. add you configurations in the .env file` -> you need to add the database configuration to go on

`4. php artisan key:generate` -> generate application keys

`5. php artisan serve or valet link` -> start the server


### Create a new micro app

#### Example
`1. Create a new folder inside the app/Http/Controllers with name App<micro-app-name>, for example AppApi`

`2. Create a new base controller with app name, for example ApiController`

`3. The content of the new base controller should be:`

```
<?php

namespace App\Http\Controllers\Api;

use App\Traits\ApiTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ApiTrait;
}

```

`This class will be extended by other controllers inside the App<micro-app-name>`

`This class may contains traits, in this case ApiTrait, may contain other classes 
and all childs of this base controller will have these`


`4. php artisan make:controller AppApi/ExampleController` -> Create a new controller inside this App<mirco-app-name>

`5. The ExampleController should extend ApiController or the new base controller`

```
<?php

namespace App\Http\Controllers\AppApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExampleController extends ApiController
{
    //
}
```
